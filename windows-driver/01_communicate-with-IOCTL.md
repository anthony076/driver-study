## IOCTL

- 為什麼需要IOCTL

  和 read() 和 write() 類似，IOCTL(IO Control)也是一個處理函數，除了可用於數據的傳輸，還可以用於傳遞控制命令和參數
  
  IOCTL函數和read()/write()一樣，可跟驅動程式交換資料，但`IOCTL交換資料的格式`，可由驅動程式開發者`自行決定`，應用較為靈活
  IOCTL函數主要用於與驅動程式通信，使app能夠與驅動程式進行更多的交互，並執行更複雜的控制和配置操作。

  read()/write()，主要用於對文件進行讀取和寫入操作。這些操作是對`應用程序中打開的文件`進行數據傳輸的標準方式

- 範例，簡易的 IOCTL driver 範例

  - ioctl.h

    ```c
    // ioctl.h

    #pragma once

    // 定義 IOCTL 代碼
    #define IOCTL_SAMPLE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800, METHOD_BUFFERED, FILE_ANY_ACCESS)

    // 定義 IOCTL 輸入緩衝區結構
    typedef struct _IOCTL_INPUT_BUFFER {
        ULONG Data; // 輸入資料
    } IOCTL_INPUT_BUFFER, *PIOCTL_INPUT_BUFFER;

    // 定義 IOCTL 輸出緩衝區結構
    typedef struct _IOCTL_OUTPUT_BUFFER {
        ULONG Result; // 輸出結果
    } IOCTL_OUTPUT_BUFFER, *PIOCTL_OUTPUT_BUFFER;
    ```

  - driver.c

    ```c
    // driver.c
    #include <ntddk.h>
    #include "ioctl.h"

    // IOCTL 處理函數
    NTSTATUS SampleIoctlHandler(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp) {
        UNREFERENCED_PARAMETER(DeviceObject);

        NTSTATUS status = STATUS_SUCCESS;
        PIO_STACK_LOCATION irpStack;
        PVOID inputBuffer, outputBuffer;
        ULONG inputBufferLength, outputBufferLength;

        // 獲取 IRP 堆疊位置
        irpStack = IoGetCurrentIrpStackLocation(Irp);

        // 確定 IOCTL 代碼
        switch (irpStack->Parameters.DeviceIoControl.IoControlCode) {
        case IOCTL_SAMPLE:
            // 檢索輸入和輸出緩衝區
            inputBuffer = Irp->AssociatedIrp.SystemBuffer;
            inputBufferLength = irpStack->Parameters.DeviceIoControl.InputBufferLength;
            outputBuffer = Irp->AssociatedIrp.SystemBuffer;
            outputBufferLength = irpStack->Parameters.DeviceIoControl.OutputBufferLength;

            // 驗證緩衝區大小
            if (inputBufferLength < sizeof(IOCTL_INPUT_BUFFER) ||
                outputBufferLength < sizeof(IOCTL_OUTPUT_BUFFER)) {
                status = STATUS_INVALID_PARAMETER;
                break;
            }

            // 處理 IOCTL 請求
            PIOCTL_INPUT_BUFFER input = (PIOCTL_INPUT_BUFFER)inputBuffer;
            PIOCTL_OUTPUT_BUFFER output = (PIOCTL_OUTPUT_BUFFER)outputBuffer;

            // 執行某些操作（例如：將輸入資料乘以2）
            output->Result = input->Data * 2;

            // 設置輸出緩衝區的實際大小
            Irp->IoStatus.Information = sizeof(IOCTL_OUTPUT_BUFFER);

            break;

        default:
            // 不支援的 IOCTL 代碼
            status = STATUS_INVALID_DEVICE_REQUEST;
            break;
        }

        // 完成 IRP
        Irp->IoStatus.Status = status;
        IoCompleteRequest(Irp, IO_NO_INCREMENT);

        return status;
    }

    // DLL 入口函數
    NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath) {
        UNREFERENCED_PARAMETER(RegistryPath);

        // 內容省略

        // Register IOCTL handler
        DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = SampleIoctlHandler;

        // 內容省略

        return STATUS_SUCCESS;
    }
    ```


## Ref

- [實際撰寫驅動程式](https://www.cntofu.com/book/46/linux_device_driver_programming/shi_ji_zhuan_xie_qu_dong_cheng_shi.md)

