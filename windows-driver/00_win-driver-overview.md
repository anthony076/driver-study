
## windows 驅動程式概述

- 驅動程式類型
  - `類型1`，使用者模式驅動程式: 在使用者模式中執行
  
  - `類型2`，核心模式驅動程式: 以核心模式執行
    - 管理I/O
    - 隨插即用記憶體
    - 進程和執行緒
    - 安全性等核心模式作業系統元件
  
  - `類型3`，WDM驅動程式: 為plug-and-play型裝置開發的驅動程式
    - windows 98 以後的驅動程式模型
    - 同時支援 kernel-mode-driver 和 kernel-mode-driver
    - 引入了統一的 I/O 模型，允許驅動程式通過 I/O 管道進行通信

- 基本概念

  - 載入驅動程式時，會立刻產生唯一的driver-Object，每個裝置必須建立自己的device-object

    不同的裝置可以使用同一份的趨動，例如，可以同時有多個USB裝置

  - 驅動程式與註冊表

    驅動程式在安裝時，系統都會產生一個註冊表項目(位於CurrentControlSet\Services\)，該項目就是當Windows系統啟動時，用來載入驅動程式使用的，Windows系統依據註冊表來決定哪些驅動程式需要被載入以及載入的順序

  - 整合型驅動程式需要建立裝置的實體對象
    
    整合型驅動程式必須為每個處理 I/O 要求的實體、邏輯或虛擬裝置建立裝置物件。
    沒有為裝置建立裝置物件的驅動程式不會收到裝置的任何 IRP。

- 驅動程式調用流程
  - 示意圖
    <img src="doc/call-flow.png" width=800 height=auto>

  - step1，用戶發起IO操作請求
  - step2，OS將該請求包裝為 IRP 結構的對象
  - step3，將IRP對象傳遞給對應驅動程式的入口函數
    - `DriverEntry()`: 驅動程式的入口函數
      - step，(選用)建立驅動對象，`IoCreateDriver()`，通常不需要手動調用
      - step，(必要)創建設備對象，`IoCreateDevice()`
      - step，註冊回調函數
        - 註冊驅動卸載函數，`DriverObject->DriverUnload`
        - 註冊文件創建函數，`DriverObject->MajorFunction[IRP_MJ_CREATE]`
        - 註冊文件關閉函數，`DriverObject->MajorFunction[IRP_MJ_CLOSE]`
        - 註冊文件讀取函數，`DriverObject->MajorFunction[IRP_MJ_READ]`

  - step4，在讀取函數中调用 `IoCompleteRequest()`，来通知操作系统 I/O 操作已经完成。

- 常用方法和對象
  - `DriverObject`: 驅動程式對象

  - `DeviceObject`: 裝置對象

  - `IoRequestObject(IRP)`: IO請求對象
    - 进行输入/输出操作的数据结构
    - 操作系统内核中对 I/O 操作的请求信息
    - 用于跟踪和控制 I/O 操作的其他信息
    - IoManager 會建立一個 fileObj ，並找到對應的 deviceObj 和 driverObj
    - dervier中定義的處理程式，會對 fileObj 進行相關操作，而不是直接操作 deviceObj 和 driverObj
      > createFile()會返回 fileObj 的 handler，以便對 fileObj 進行操作 (!! EVERYTHING IS FILE !!)

      > 透過 fileObj 與 hardware 進行交互

  - `DriverEntry(PDRIVER_OBJECT, PUNICODE_STRING)`
    - 輸入參數 PDRIVER_OBJECT: 指向該驅動程式的資料位置，該位置會包含驅動程式的所有資訊
    - 輸入參數 PUNICODE_STRING: 該驅動程式的Registry位置
  
## 專有名詞

| 名稱 | 全稱                         | 用途                                              |
| ---- | ---------------------------- | ------------------------------------------------- |
| IRP  | I/O Request Packet           | 所有 I/O 要求都會以 IRP對象的形式，傳送給驅動程式 |
| DCB  | Device Control Block         | 串口通訊時，用於保存通訊參數的對象                |
| WDM  | Windows Driver Model         |                                                   |
| WDF  | Windows Driver Frameworks    | 開發 windows-driver 的框架                        |
| KMDF | Kernel Mode Driver Framework | 簡化 kernel-mode-driver 開發過程的框架            |
| UMDF | User Mode Driver Framework   | 簡化 user-mode-driver 開發過程的框架              |

## Ref

- [Kernel-Mode驅動程式架構設計指南](https://learn.microsoft.com/zh-tw/windows-hardware/drivers/kernel/)
- [How Windows Driver Works @ Programming LoL](https://www.youtube.com/watch?v=3ffZbDB-pg4)\
- [Windows Driver Development * 19 @ Programming LoL](https://youtube.com/playlist?list=PLZ4EgN7ZCzJyUT-FmgHsW4e9BxfP-VMuo&si=mvp1h08U7iVB5e-N)
