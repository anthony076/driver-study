
## 範例，載入驅動後，打印出字串範例

- step1，編寫代碼
  
  ```c
  // example.c
  #define _AMD64_
  #include <wdm.h>

  NTSTATUS DriverEntry(void* a, void* b)
  {
    DbgPrint("Hello from Nirs Drivers!");
    return STATUS_SUCCESS;
  }
  ```

- step2，透過 Visual Studio 進行編譯
  - 利用 `cl.exe` 進行編譯，位於`"C:Programs Files (x86)\Microsoft Visual Studio\2022\BuildTools\"`
  - 代碼中的 `DbgPrint()` 位於`NtosKrnl.lib`的檔案中
  - 編譯命令
    ```batch
    @REM 要編譯的檔案
    cl example.c "C:Programs Files (x86)\Windows Kits\10\Lib\10.0.22621\x64\NtosKrnl.lib" \
      @REM 指定header的位置
      /I "C:Programs Files (x86)\Windows Kits\10\Lib\10.0.22621\km" \
      @REM 指定連結參數
      /link /subsystem:native \
      @REM 指定驅動類型
      /driver:wdm \
      @REM 指定驅動的入口函數
      -entry:DriverEntry
    ```
    
    此編譯命令會產生 example.exe 和其他相關檔案

  - step3，將`example.exe`更名為`example.sys`，`$ ren example.exe example.sys`

  - step4，設置系統模式，該模式會忽略驅動程式的簽名
    - 按下作業系統的 `shift + Restart 鍵` 進入作業系統選單
    - TroubleShooting > Advanced options > Startup Settings > 按下 restart 按鈕
    - 按下7，啟用 Disable driver signature enforcement
    - 自動重新啟動系統

  - 註冊和測試驅動程式
    - 以管理員打開cmd
    - 註冊驅動程式命令，`sc create testDriver binPath= C:\path\to\example.c type= kernel`
    - 開啟 DbgView.exe，以查看DbgPrint()打印出的訊息
      - 選擇 Capture > Enable Verbose Kernel Output 打勾
      - 按下 ctrl+x 清除訊息
    - 啟動驅動程式命令，`sc start testDriver`
    - 刪除驅動程式命令，`sc delete testDriver`

## 範例，USB 與 Arduino 通訊範例 (not driver)

以下範例不是透過 driver 進行通訊，但是可以將執行檔註冊為service或開機自動啟動程式(start-program)，
以實現自動執行的目的

- step1，代碼

  ```c
  // example.c

  #include <windows.h>
  #include <stdio.h>

  // 定義串口句柄
  HANDLE hSerial;

  // 初始化串口
  void initSerial() {

    // 建立串口的handler
    hSerial = CreateFile("\\\\.\\COM3", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

    // 檢查串口是否啟動正常
    if (hSerial == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "無法打開串口\n");
        exit(1);
    }

    // 串口通訊參數，dcbSerialParams，Device Control Block
    DCB dcbSerialParams = { 0 };
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

    // 獲取串口狀態
    if (!GetCommState(hSerial, &dcbSerialParams)) {
        fprintf(stderr, "獲取串口狀態失敗\n");
        CloseHandle(hSerial);
        exit(1);
    }

    // 設置與 arduino 通過 usb 通訊的串口參數
    dcbSerialParams.BaudRate = CBR_9600;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity = NOPARITY;

    if (!SetCommState(hSerial, &dcbSerialParams)) {
        fprintf(stderr, "設定串口狀態失敗\n");
        CloseHandle(hSerial);
        exit(1);
    }
  }

  // 讀取串口數據
  char readSerial() {
    char byte;
    DWORD bytesRead;

    ReadFile(hSerial, &byte, sizeof(byte), &bytesRead, NULL);

    return byte;
  }

  // 關閉串口
  void closeSerial() {
    CloseHandle(hSerial);
  }

  int main() {
    initSerial();

    while (1) {
        char buttonState = readSerial();

        // 檢查按鈕狀態，此處假設按下按鈕時Arduino會發送字符'B'
        if (buttonState == 'B') {
            system("start cmd");
        }
    }

    closeSerial();

    return 0;
  }
  ```

- step2，將以上代碼編譯為 exe，`gcc example.c -o example.exe`

- step3，(optional) 添加到開機自動啟動，`C:\Users\<你的用户名>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup`

- step3，(optional) 添加為服務

  - 打開 regedit
  - 定位到 `HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run`
  - 在右側新增鍵值
    - 鍵: MyProgram | 字符串值
    - 值: `C:\path\to\example.exe`


## 範例，USB 與 Arduino 通訊範例

- step1，代碼

  ```c
  #include <ntddk.h>

  #define ARDUINO_BUTTON_STATE 0x01  // 根據需要更改按鈕狀態的定義

  // 驅動程式入口函數
  extern "C" NTSTATUS
  DriverEntry( _In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath)
  {
    UNREFERENCED_PARAMETER(RegistryPath);

    DriverObject->DriverUnload = UnloadDriver;

    // 創建設備對象
    
    UNICODE_STRING devName;
    RtlInitUnicodeString(&devName, L"\\Device\\ArduinoButton");

    PDEVICE_OBJECT devObj;
    NTSTATUS status = IoCreateDevice(DriverObject, 0, &devName, FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &devObj);

    if (!NT_SUCCESS(status)) {
        KdPrint(("Failed to create device object\n"));
        return status;
    }

    // 設定 IRP 處理函數
    DriverObject->MajorFunction[IRP_MJ_CREATE] = CreateCloseDispatch;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = CreateCloseDispatch;
    DriverObject->MajorFunction[IRP_MJ_READ] = ReadDispatch;

    // 設定設備對象的 Flags
    devObj->Flags |= DO_BUFFERED_IO;

    // 創建符號鏈接，以便用戶空間應用程式可以訪問設備
    UNICODE_STRING symLink;
    RtlInitUnicodeString(&symLink, L"\\DosDevices\\ArduinoButton");
    IoCreateSymbolicLink(&symLink, &devName);

    return STATUS_SUCCESS;
  }

  // IRP_MJ_CREATE 和 IRP_MJ_CLOSE 處理函數
  extern "C" NTSTATUS CreateCloseDispatch( _In_ PDEVICE_OBJECT DeviceObject, _In_ PIRP Irp)
  {
      UNREFERENCED_PARAMETER(DeviceObject);

      Irp->IoStatus.Status = STATUS_SUCCESS;
      Irp->IoStatus.Information = 0;

      IoCompleteRequest(Irp, IO_NO_INCREMENT);

      return STATUS_SUCCESS;
  }

  // IRP_MJ_READ 處理函數
  extern "C" NTSTATUS ReadDispatch( _In_ PDEVICE_OBJECT DeviceObject, _In_ PIRP Irp)
  {
      UNREFERENCED_PARAMETER(DeviceObject);

      // TODO: 在此處添加讀取 Arduino 按鈕狀態的代碼

      // 如果檢測到按鈕按下，啓動 cmd
      if (ArduinoButtonPressed()) {
          system("start cmd");
      }

      Irp->IoStatus.Status = STATUS_SUCCESS;
      Irp->IoStatus.Information = 0;

      IoCompleteRequest(Irp, IO_NO_INCREMENT);

      return STATUS_SUCCESS;
  }

  // 卸載驅動程式
  extern "C" VOID UnloadDriver( _In_ PDRIVER_OBJECT DriverObject)
  {
      // 刪除設備對象和符號鏈接
      UNICODE_STRING symLink;
      RtlInitUnicodeString(&symLink, L"\\DosDevices\\ArduinoButton");
      IoDeleteSymbolicLink(&symLink);

      IoDeleteDevice(DriverObject->DeviceObject);

      KdPrint(("Driver unloaded\n"));
  }
  ```

## Ref

- [Making Simple Windows Driver in C @ Nir Lichtman](https://www.youtube.com/watch?v=GTrekHE8A00)